import base_test

class TestCases(base_test.BaseTest):

    def test_01_test_login_page_login(self):
        #Step 1: Click on login and the login page loads
        assert self.welcomePage.click_login(self.loginPage)
        #Step 2: Login to the webpage and confirm the main page appears
        assert self.loginPage.login(self.mainPage, "testuser", "testing")
        # #Step 3: Logout and confirm the welcome page appears
        # assert self.mainPage.click_logout(self.welcomePage)
