import unittest

import time

from utils import Parameters
from page10_welcome_page import WelcomePage
from page20_login_page import LoginPage
from page30_main_page import MainPage
from page40_frames_page import FramePage

class BaseTest(unittest.TestCase):
    param = Parameters()
    welcomePage = WelcomePage(param.w, param.rootUrl)
    loginPage = LoginPage(param.w,param.rootUrl)
    mainPage = MainPage(param.w,param.rootUrl)
    framePage = FramePage(param.w,param.rootUrl)
    a="object"
    def setUp(self):
        self.param.w.get(self.param.rootUrl)
        self.param.w.maximize_window()
        assert self.welcomePage.check_page()
    @classmethod
    def tearDownClass(cls):
        time.sleep(2)
        cls.param.w.quit()
